package ru.tsc.karbainova.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.karbainova.tm.endpoint.*;

@Configuration
@ComponentScan("ru.tsc.karbainova.tm")
public class ContextConfiguration {
    @Bean
    @NotNull
    public AdminUserEndpoint adminEndpoint() {
        return new AdminUserEndpointService().getAdminUserEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

}
