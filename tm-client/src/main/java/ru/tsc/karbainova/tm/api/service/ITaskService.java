package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.endpoint.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void clear();

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, TaskDTO task);

    void remove(String userId, TaskDTO task);

    List<TaskDTO> findAll();

    List<TaskDTO> findAll(String userId);

    List<TaskDTO> findAll(String userId, Comparator<TaskDTO> comparator);

    TaskDTO updateById(String userId, String id, String name, String description);

    TaskDTO updateByIndex(String userId, Integer index, String name, String description);

    TaskDTO removeById(String userId, String id);

    TaskDTO removeByIndex(String userId, Integer index);

    TaskDTO removeByName(String userId, String name);

    TaskDTO findById(String userId, String id);

    TaskDTO findByIndex(String userId, Integer index);

    TaskDTO findByName(String userId, String name);

    TaskDTO startById(String userId, String id);

    TaskDTO startByIndex(String userId, Integer index);

    TaskDTO startByName(String userId, String name);

    TaskDTO finishById(String userId, String id);

    TaskDTO finishByIndex(String userId, Integer index);

    TaskDTO finishByName(String userId, String name);

    void addAll(List<TaskDTO> tasks);
}
