package ru.tsc.karbainova.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.endpoint.ProjectEndpoint;


public abstract class AbstractListenerProject extends AbstractSystemListener {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

}
