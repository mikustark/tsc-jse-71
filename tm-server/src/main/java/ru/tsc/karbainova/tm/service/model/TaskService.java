package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.model.TaskRepository;
import ru.tsc.karbainova.tm.api.service.model.ITaskServiceModel;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskServiceModel {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @SneakyThrows
    public List<Task> findAllTaskByUserId(String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Transactional
    @SneakyThrows
    public void addAll(Collection<Task> collection) {
        if (collection == null) return;
        for (Task i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        taskRepository.clear();
    }

    @Override
    @Transactional
    @SneakyThrows
    public Task add(Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        final User user = new User();
        user.setId(userId);
        task.setUser(user);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.removeByIdUserId(userId, task.getId());
    }

    @Override
    @Transactional
    @SneakyThrows
    public Task updateById(@NonNull String userId, @NonNull String id,
                           @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = taskRepository.findByIdUserId(userId, id);
        if (task == null) throw new ProjectNotFoundException();
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @Override
    @SneakyThrows
    public Task findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public Task findByIdUserId(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByIdUserId(userId, id);
    }
}
