package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.dto.IUserService;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    protected IUserService userService;

    @WebMethod
    public UserDTO createUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password) {
        sessionService.validate(session);
        return userService.create(login, password);
    }

    @WebMethod
    public UserDTO setPasswordUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "password") String password) {
        sessionService.validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserDTO createUserWithEmail(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email) {
        sessionService.validate(session);
        return userService.create(login, password, email);
    }

    @WebMethod
    public UserDTO updateUserUser(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "firstName") String firstName,
            @WebParam(name = "lastName") String lastName,
            @WebParam(name = "middleName") String middleName) {
        sessionService.validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }
}
