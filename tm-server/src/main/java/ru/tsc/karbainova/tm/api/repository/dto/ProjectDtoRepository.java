package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.dto.ProjectDTO;

import java.util.List;

public interface ProjectDtoRepository extends JpaRepository<ProjectDTO, String> {

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDTO findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    long countByUserId(@NotNull String userId);
}

