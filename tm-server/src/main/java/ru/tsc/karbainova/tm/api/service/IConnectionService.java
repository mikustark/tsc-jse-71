package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NonNull EntityManager getEntityManager();
}
