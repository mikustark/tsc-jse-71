# TASK MANAGER

## DEVELOPER INFO

name: Mariya Karbainova

e-mail: mariya@karbainova

e-mail: mariya@yandex.ru

## HAEDWARE

CPU: i5

RAM: 16G

SSD: 512GB

## SOFTWARE

Version JDK: 1.8.0_281

## PROGRAM RUN

```bash
mvn clean install
```

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
