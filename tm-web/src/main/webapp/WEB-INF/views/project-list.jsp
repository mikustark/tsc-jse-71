<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Desc</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th align="center">Edit</th>
        <th align="center">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.startDate}"/>
            </td>
            <td>
                <c:out value="${project.finishDate}"/>
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/project/create">
    <button type="submit">Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
