<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th align="center">Edit</th>
        <th align="center">Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.startDate}" />
            </td>
            <td>
                <c:out value="${task.finishDate}" />
            </td>
            <td>
                <a href="/task/edit/${task.id}">Edit</a>
            </td>
            <td>
                <a href="/task/delete/${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/task/create">
    <button type="submit">Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
