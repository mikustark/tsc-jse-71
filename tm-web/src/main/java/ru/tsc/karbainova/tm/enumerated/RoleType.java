package ru.tsc.karbainova.tm.enumerated;

public enum RoleType {
    USER,
    ADMIN;
}
