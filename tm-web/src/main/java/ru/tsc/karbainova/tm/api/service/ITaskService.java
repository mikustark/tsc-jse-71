package ru.tsc.karbainova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {
    List<Task> findAll();

    List<Task> findAll(@NotNull String userId);

    void addAll(Collection<Task> collection);

    void addAll(String userId, @Nullable Collection<Task> collection);

    Task save(Task entity);

    @Nullable
    Task save(String userId, @Nullable Task entity);

    void create();

    void create(String userId);

    Task findById(String id);

    Task findById(@NotNull String userId, @Nullable String id);

    void clear();

    void clear(@NotNull String userId);

    void removeById(String id);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(Task entity);

}
