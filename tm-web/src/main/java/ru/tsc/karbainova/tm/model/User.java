package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.karbainova.tm.enumerated.Status;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User {

    @Id
    protected String id = UUID.randomUUID().toString();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date created = new Date();

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date endDate;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date startDate;

    protected String description = "";

    protected String name = "";

    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column(name = "user_id")
    private String userId;

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column(columnDefinition="boolean DEFAULT false")
    private boolean lock = false;

    public boolean isLock() {
        return lock;
    }
}
