package ru.tsc.karbainova.tm.exception;

import org.jetbrains.annotations.NotNull;

public class AccessDeniedException extends AbstractException{
    @NotNull
    public AccessDeniedException() {
        super("Error. Access denied.");
    }
}
