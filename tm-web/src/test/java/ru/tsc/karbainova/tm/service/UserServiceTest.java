package ru.tsc.karbainova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.karbainova.tm.api.service.IUserService;
import ru.tsc.karbainova.tm.config.ApplicationConfig;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfig.class})
public class UserServiceTest {
    @NotNull
    @Autowired
    private IUserService userService;

    @Nullable
    private User user;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private static String USER_ID;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        @NotNull final User user = new User();
        user.setLogin("UserTest");
        user.setEmail("email");
        user.setPasswordHash("1");
        this.user = userService.add(user);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("UserTest", user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals("email", user.getEmail());
        @Nullable final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertTrue(users.size() > 1);
    }

    @Test
    public void findById() {
        @Nullable final User user = userService.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final User user = userService.findById("34");
        Assert.assertNull(user);
    }

    @Test
    public void remove() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Test
    public void removeById() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

}
